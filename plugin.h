#pragma once

#include <QObject>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "DataExtention.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "PLAG.Plugin" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	virtual ~Plugin() override;

private:
	DataExtention* m_dataExtention;
};
