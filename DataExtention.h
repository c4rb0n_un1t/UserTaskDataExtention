#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IUserTaskDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IUserTaskDataExtention IDataExtention)

	DATA_EXTENTION_BASE_DEFINITIONS(IUserTaskDataExtention, IUserTaskDataExtention, {"name", "isDone"})

public:
	DataExtention(QObject* parent) :
		QObject(parent)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

public:
	QString name() override
	{
		return "New Task";
	}

	bool isDone() override
	{
		return false;
	}
};
